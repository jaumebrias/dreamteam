﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PracticaProgramacio
{
    public class Posicio : UserControl
    {
        protected string nom;
        protected int fila;
        protected int columna;
        protected Grid container;
        protected Image imgIcona;
        protected TextBlock tbNom;

        #region Constructors
        public Posicio(string nom, int fila, int columna)
        {
            this.nom = nom;
            this.fila = fila;
            this.columna = columna;
            tbNom = new TextBlock();
            if (nom != null && nom != "")
            {
                tbNom.Text = nom;
                tbNom.TextAlignment = TextAlignment.Center;
            }
            imgIcona = new Image();
            

            container = new Grid();
            

            RowDefinition rd;
            for (int i = 0; i < 2; i++)
            {
                rd = new RowDefinition();
                container.RowDefinitions.Add(rd);
            }
            
            container.Children.Add(imgIcona);
            container.Children.Add(tbNom);
            container.VerticalAlignment = VerticalAlignment.Center;
            container.HorizontalAlignment = HorizontalAlignment.Center;
            Grid.SetRow(imgIcona, 0);
            Grid.SetRow(tbNom, 1);
           
            this.AddChild(container);

        }

        

        public Posicio(int fila, int columna) : this("", fila, columna) { }
        public Posicio(string nom) : this(nom, 0, 0){ }
        public Posicio() : this("", 0, 0){ }
        #endregion

        #region Propietats
        public string Nom { get => nom;
            set
            {
                nom = value;
                tbNom.Text = nom;
            }
        }
        public int Columna { get => columna; set => columna = value; }
        public int Fila { get => fila; set => fila = value; }
        public virtual bool EsBuida
        {
            get
            {
                return true;
            }
        }
        public ImageSource ImgIcona { get => imgIcona.Source; set => imgIcona.Source = value; }
        public Image Img {
            set
            {
                imgIcona.Visibility = Visibility.Hidden;
                container.Children.Add(value);
                Grid.SetRow(value, 0);
            }
        }
        //public TextBlock TbNom { get => tbNom; set => tbNom = value; }
        #endregion

        public static double Distancia(Posicio pos1,Posicio pos2)
        {
            double dinstancia;
            dinstancia = Math.Sqrt(Math.Pow((pos2.Columna - pos1.Columna), 2) + Math.Pow((pos2.Fila - pos1.Fila), 2));
            return dinstancia;
        }
        
    }
}
