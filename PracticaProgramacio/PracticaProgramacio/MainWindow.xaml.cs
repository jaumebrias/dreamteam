﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PracticaProgramacio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int nFiles;
        int nColumnes;
        int nHomes;
        int nDones;
        int nCambrers;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            nFiles = (int)sldNFiles.Value;
            nColumnes = (int)sldNColumnes.Value;
            nHomes = (int)sldNHomes.Value;
            nDones = (int)sldNDones.Value;
            nCambrers = (int)sldNCambrers.Value;

            if (nFiles * nColumnes < nHomes + nDones + nCambrers) MessageBox.Show("Hi han mes persones que llocs disponibles");
            else
            {
                Escenari esc = new Escenari(nFiles, nColumnes, nHomes, nDones, nCambrers);
                Tauler taul = new Tauler(esc);
                taul.ShowDialog();
            }

        }
    }
}
