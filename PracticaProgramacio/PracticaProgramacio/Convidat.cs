﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaProgramacio
{
    public abstract class Convidat : Persona
    {
        protected int plusSexe;
        protected SortedDictionary<String, Int32> simpaties;
        public Convidat() { }
        public Convidat(String nom, int plusSexe) : this(nom, plusSexe, new SortedDictionary<string, int>()) { }
        public Convidat(String nom, int plusSexe, SortedDictionary<string, int> simpaties) : base(nom)
        {
            this.plusSexe = plusSexe;
            this.simpaties = simpaties;
        }
        public abstract bool EsHome { get; }
        public override bool EsConvidat { get => true; }
        public SortedDictionary<String, Int32> Simpaties { get => simpaties; set => simpaties = value; }
        public int PlusSexe { get => plusSexe;
            set
            {
                if (value < 0) value = 0;
                else if (value > 3) value = 3;
                
                plusSexe = value;
            } }
        
    }
}
