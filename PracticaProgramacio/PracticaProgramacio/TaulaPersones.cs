﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaProgramacio
{
    public class TaulaPersones : List<Persona>
    {
        int NCambrers;
        int NHomes;
        int NDones;
        private Random r = new Random();
        List<Convidat> llistaConvidats = new List<Convidat>();
        SortedDictionary<String, Int32> simpaties = new SortedDictionary<String, Int32>();

        private void CreaSimpaties(Convidat nou)
        {
            //Random r = new Random();
            int aleatori = r.Next(-5, 5);
            SortedDictionary<String, Int32> aux = new SortedDictionary<String, Int32>(simpaties);
            foreach (string nom in simpaties.Keys)
            {
                //nou.Simpaties[nom] = r.Next(-5, 5);
                aux[nom] = r.Next(-5, 5);
            }
            nou.Simpaties = aux;
            simpaties.Add(nou.Nom, 0);
            //LA KEY ES EL NOM DE LA PERSONA
            //TOTES LES SIMPATIES ES CREEN AMB RANDOM
        }
        public void AfegeixPersona(Persona persona)
        {
            if (persona.EsConvidat)
            {
                CreaSimpaties((Convidat)persona);

                foreach (Convidat c in llistaConvidats)
                {
                    c.Simpaties.Add(persona.Nom, r.Next(-5, 5));
                }

                llistaConvidats.Add((Convidat)persona);

            }
            this.Add(persona);
        }
        public void EliminaPersona(Persona persona)
        {
            //si es un convidat
            if (persona.EsConvidat)
            {
                foreach (Convidat c in llistaConvidats)
                {
                    c.Simpaties.Remove(persona.Nom);
                }
                llistaConvidats.Remove((Convidat)persona);
            }
            //independentment si es convidat o cambrer
            this.Remove(persona);
        }

        public List<Convidat> LlistaConvidats
        {
            get
            {
                return llistaConvidats;
            }
        }
        public SortedDictionary<String, Int32> Simpaties
        {
            get
            {
                return simpaties;
            }
        }
    }
}
