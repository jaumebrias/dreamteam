﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace PracticaProgramacio
{
    /// <summary>
    /// Interaction logic for Simpaties.xaml
    /// </summary>
    public partial class Simpaties : Window
    {
        Escenari escenari;

        public Simpaties(Escenari escenari)
        {
            InitializeComponent();
            this.escenari = escenari;
        }

        private void CrearGrid(List<Convidat> llistaConvidats)
        {
            Grid grid = new Grid();
           
           
            grid.ShowGridLines = false;
            grid.Background = new SolidColorBrush(Colors.White);

            RowDefinition rd;
            ColumnDefinition cd;
            //Afegim tantes rd i cd com convidats + 1 a la llista
            for (int i = 0; i <= llistaConvidats.Count; i++)
            {
                cd = new ColumnDefinition();
                rd = new RowDefinition();
                rd.Height = new GridLength(1, GridUnitType.Star);
                cd.Width = new GridLength(1, GridUnitType.Star);
                grid.RowDefinitions.Add(rd);
                grid.ColumnDefinitions.Add(cd);
            }

            //Capçalera
            TextBlock txt;
            UserControl cela;
            for (int i = 0; i <= llistaConvidats.Count; i++)
            {
                cela = new UserControl();
                txt = new TextBlock();
                if (i == 0)
                    txt.Text = "CONVIDAT";
                else
                    txt.Text = llistaConvidats[i - 1].Nom;
                txt.FontSize = 20;
                txt.FontWeight = FontWeights.Bold;
                txt.Foreground = new SolidColorBrush(Colors.Black);
                txt.Background = new SolidColorBrush(Colors.White);
                txt.VerticalAlignment = VerticalAlignment.Top;

                
                if(i > 0)
                {
                    //Modificar plusSexe
                    Convidat c = (Convidat)llistaConvidats[i - 1];
                    Grid gridContenidorSexe = new Grid();
                    RowDefinition rdContenidorSexe = new RowDefinition();
                    rdContenidorSexe.Height = new GridLength(1, GridUnitType.Star);
                    RowDefinition rdContenidorSexe2 = new RowDefinition();
                    rdContenidorSexe2.Height = new GridLength(1, GridUnitType.Star);
                    gridContenidorSexe.RowDefinitions.Add(rdContenidorSexe);
                    gridContenidorSexe.RowDefinitions.Add(rdContenidorSexe2);

                    IntegerUpDown iudSexe = new IntegerUpDown();
                    iudSexe.BorderBrush = Brushes.Transparent;
                    iudSexe.Value = c.PlusSexe;
                    iudSexe.FontSize = 25;

                    iudSexe.Minimum = 0;
                    iudSexe.Maximum = 3;
                    iudSexe.FontWeight = FontWeights.Bold;
                    iudSexe.Foreground = new SolidColorBrush(Colors.Black);
                    iudSexe.Background = new SolidColorBrush(Colors.White);
                    iudSexe.VerticalAlignment = VerticalAlignment.Top;
                    iudSexe.HorizontalAlignment = HorizontalAlignment.Right;
                    iudSexe.Tag = llistaConvidats[i-1];
                    iudSexe.ValueChanged += new RoutedPropertyChangedEventHandler<object>(IntegerUpDownValueChangedSexe);
                    gridContenidorSexe.Children.Add(txt);
                    gridContenidorSexe.Children.Add(iudSexe);
                    Grid.SetRow(txt, 0);
                    Grid.SetRow(iudSexe, 1);

                    
                    if (c.EsHome)
                    {
                        cela.Background = Brushes.LightBlue;
                        iudSexe.Background = Brushes.LightBlue;
                        txt.Background = Brushes.LightBlue;
                    }
                    else
                    {
                        cela.Background = Brushes.LightCoral;
                        iudSexe.Background = Brushes.LightCoral;
                        txt.Background = Brushes.LightCoral;
                    }

                    cela.Content = gridContenidorSexe;
                }

                cela.BorderBrush = new SolidColorBrush(Colors.Black);
                cela.BorderThickness = new Thickness(0.5);
                grid.Children.Add(cela);
                Grid.SetRow(cela, 0);
                Grid.SetColumn(cela, i);
            }

            //Primera columna
            for (int i = 1; i <= llistaConvidats.Count; i++)
            {
                cela = new UserControl();
                txt = new TextBlock();
                txt.Text = llistaConvidats[i - 1].Nom;

                Convidat c = (Convidat)llistaConvidats[i - 1];
                if(c.EsHome)
                {
                    cela.Background = Brushes.LightBlue;
                    txt.Background = Brushes.LightBlue;
                }
                else
                {
                    cela.Background = Brushes.LightCoral;
                    txt.Background = Brushes.LightCoral;
                }
                txt.FontSize = 20;
                txt.FontWeight = FontWeights.Bold;
                txt.Foreground = new SolidColorBrush(Colors.Black);
                txt.VerticalAlignment = VerticalAlignment.Top;

                cela.Content = txt;
                cela.BorderBrush = new SolidColorBrush(Colors.Black);
                cela.BorderThickness = new Thickness(0.5);
                grid.Children.Add(cela);
                Grid.SetRow(cela, i);
                Grid.SetColumn(cela, 0);
            }

            int counter = 0;
            IntegerUpDown iud; //Haura de ser un IntegerUpDown
            Convidat actual;
            Convidat aux;
            for (int fila = 1; fila <= llistaConvidats.Count; fila++)
            {
                counter = 0;
                actual = llistaConvidats[fila - 1];
                for (int columna = 1; columna <= llistaConvidats.Count; columna++)
                {
                    cela = new UserControl();
                    iud = new IntegerUpDown();
                    iud.BorderBrush = Brushes.Transparent;
                    iud.Minimum = -5;
                    iud.Maximum = 5;
                    aux = llistaConvidats[counter];

                    if (llistaConvidats[counter].Nom == actual.Nom) //Si es tracta d'ell mateix no busquem a la taula de simpaties
                        iud.IsEnabled = false;
                    else
                        iud.Value = actual.Simpaties[llistaConvidats[counter].Nom];
                    iud.FontSize = 25;
                    iud.FontWeight = FontWeights.Bold;
                    iud.Foreground = new SolidColorBrush(Colors.Black);
                    iud.Background = new SolidColorBrush(Colors.White);
                    iud.VerticalAlignment = VerticalAlignment.Top;
                    iud.HorizontalAlignment = HorizontalAlignment.Right;
                    KeyValuePair<Convidat, String> personaINom = new KeyValuePair<Convidat, string>(actual, llistaConvidats[counter].Nom);
                    iud.ValueChanged += new RoutedPropertyChangedEventHandler<object>(IntegerUpDownValueChanged);
                    iud.Tag = personaINom;


                    cela.Content = iud;
                    cela.BorderBrush = new SolidColorBrush(Colors.Black);
                    cela.BorderThickness = new Thickness(0.5);
                    Grid.SetRow(cela, fila);
                    Grid.SetColumn(cela, columna);
                    grid.Children.Add(cela);
                    counter++;
                }
            }
            containerTaulaSimpaties.Children.Add(grid);
      
        }

        private void IntegerUpDownValueChangedSexe(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            IntegerUpDown iud = (IntegerUpDown)sender;
            int? value = iud.Value;
            if(value != null)
            {
                Convidat c = (Convidat)iud.Tag;
                c.PlusSexe = (int)value;
            }

        }

        private void IntegerUpDownValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            IntegerUpDown integerUpDown = (IntegerUpDown)sender;
            int? simpatiaValor = integerUpDown.Value;
            if(simpatiaValor != null)
            {
                KeyValuePair<Convidat, String> keyValuePair = (KeyValuePair<Convidat, String>) integerUpDown.Tag;
                Convidat convidat = keyValuePair.Key;
                SortedDictionary<String, Int32> sortedDictionary = new SortedDictionary<String, Int32>(convidat.Simpaties);
                sortedDictionary[keyValuePair.Value] = (Int32) simpatiaValor;
                convidat.Simpaties = sortedDictionary;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CrearGrid(escenari.Gent.LlistaConvidats);
        }
    }
}
