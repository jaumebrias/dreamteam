﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaProgramacio
{
    public class Home : Convidat
    {
        public Home(String nom, SortedDictionary<String, Int32> simpaties, int sexe) : base(nom, sexe, simpaties) { }
        public Home(String nom, int sexe) : base (nom, sexe) { }

        public override bool EsHome => true;

        public override int Interes(Posicio pos)
        {
            if (pos.EsBuida)
                return 0;
            else
            {

                Persona per = (Persona) pos;

                if (per.Fila == this.Fila && per.Columna == this.Columna)
                    return 0;
                if (!per.EsConvidat)
                    return 1;
                else
                {
                    Convidat con = (Convidat) per;
                    if (con.EsHome)
                        return Simpaties[pos.Nom];
                    else
                        return Simpaties[pos.Nom] + PlusSexe;
                }

            }
        }
    }
}
