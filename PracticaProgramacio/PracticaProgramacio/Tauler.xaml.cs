﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PracticaProgramacio
{
    /// <summary>
    /// Interaction logic for Tauler.xaml
    /// </summary>
    public partial class Tauler : Window
    {
        private Escenari escenari;

        public Tauler(Escenari escenari)
        {
            InitializeComponent();
            this.escenari = escenari;
        }

        private void Tauler_Loaded(object sender, RoutedEventArgs e)
        {
            tauler.Children.Add(escenari);
        }

        private void BtnCicle_Click(object sender, RoutedEventArgs e)
        {
            escenari.Cicle();
        }

        private void BtnSimpaties_Click(object sender, RoutedEventArgs e)
        {
            Simpaties simpaties = new Simpaties(escenari);
            simpaties.ShowDialog();
        }

        private void BtnSortir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ImgAfegeix_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var data = new DataObject();
                switch(((Image) sender).Name)
                {
                    case "imgAfegeixHome":
                        data.SetData("tipus", "AFEGEIX_HOME");
                        break;
                    case "imgAfegeixDona":
                        data.SetData("tipus", "AFEGEIX_DONA");
                        break;
                    case "imgAfegeixCambrer":
                        data.SetData("tipus", "AFEGEIX_CAMBRER");
                        break;
                }
                DragDrop.DoDragDrop((Image) sender, data, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        private void ImgElimina_Drop(object sender, DragEventArgs e)
        {
            Persona p = (Persona)e.Data.GetData("PERSONA");
            escenari.Elimina(p);
        }

        private void ImgEgg_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TaulaPersones gent = escenari.Gent;
            String url = "";
            foreach(Persona p in gent)
            {           
                url = "/Imatges/xeviCambrer.png";
                if (p.EsConvidat)
                {
                    Convidat c = (Convidat)p;
                    if (c.EsHome)
                        url = "/Imatges/xeviHome.png";
                    else url = "/Imatges/xeviDona.png";
                }
                Image imatge = new Image()
                {
                    Source = new BitmapImage(new Uri(url, UriKind.Relative))
                };
                p.Img = imatge;
                var ease = new PowerEase { EasingMode = EasingMode.EaseOut };
                DoubleAnimation rotateAnimation = new DoubleAnimation(0, 360, new Duration(TimeSpan.FromSeconds(2)));
                rotateAnimation.RepeatBehavior = RepeatBehavior.Forever;
                RotateTransform rt = new RotateTransform();
                imatge.RenderTransform = rt;
                imatge.RenderTransformOrigin = new Point(0.5, 0.5);
                rt.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);

            }
        }

      
    }
}
