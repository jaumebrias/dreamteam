using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PracticaProgramacio
{
    public class Escenari : Grid
    {
        #region atributs
        private int files;
        private int columnes;
        private int homes;
        private int dones;
        private int cambrers;
        #endregion

        //List<List<Posicio>> matriu;
        Posicio[,] matriu;
        private Random r;
        TaulaPersones gent = new TaulaPersones();
        List<String> nomsPersones; //TODO Llista per saber quins noms han sortit i que no hi hagin de repetits
        private readonly object locker = new object();

        #region constructors
        public Escenari(int files, int columnes) : this(files, columnes, 0, 0, 0) { }

        public Escenari(int files, int columnes, int nHomes, int nDones, int nCambrers)
        {
            this.files = files;
            this.columnes = columnes;
            homes = nHomes;
            dones = nDones;
            cambrers = nCambrers;

            matriu = new Posicio[files, columnes];
            RowDefinition rd;
            ColumnDefinition cd;
            for (int fila = 0; fila < files; fila++)
            {
                rd = new RowDefinition();
                rd.Height = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star);
                this.RowDefinitions.Add(rd);
            }
            for (int columna = 0; columna < columnes; columna++)
            {
                cd = new ColumnDefinition();
                cd.Width = new GridLength(1, GridUnitType.Star);
                this.ColumnDefinitions.Add(cd);
            }

            //Afegim una posició a cada cassella del tauler
            Posicio p = null;
            r = new Random();
            String nom;

            nomsPersones = new List<string>();
            for (int fila = 0; fila < files; fila++)
            {
                for (int columna = 0; columna < columnes; columna++)
                {
                    var data = new DataObject();
                    if (nHomes > 0)
                    {
                        nom = ObteNom("Noms/NomsNenCurts.txt");
                        while (nomsPersones.Contains(nom))
                        {
                            nom = ObteNom("Noms/NomsNenCurts.txt");
                        }
                        nomsPersones.Add(nom);
                        p = new Home(nom, r.Next(3));
                        p.Background = Brushes.LightBlue;
                        ObteImatge((Persona)p);
                        nHomes--;
                        gent.AfegeixPersona((Persona)p);
                        p.AllowDrop = false;
                        gent.Add((Persona)p);
                    }
                    else if (nDones > 0)
                    {
                        nom = ObteNom("Noms/NomsNenaCurts.txt");
                        while (nomsPersones.Contains(nom))
                        {
                            nom = ObteNom("Noms/NomsNenaCurts.txt");
                        }
                        nomsPersones.Add(nom);
                        p = new Dona(nom, r.Next(3));
                        p.Background = Brushes.LightCoral;
                        ObteImatge((Persona)p);
                        nDones--;
                        gent.AfegeixPersona((Persona)p);
                        p.AllowDrop = false;
                    }
                    else if (nCambrers > 0)
                    {
                        p = new Cambrer();
                        p.Background = Brushes.LightGray;
                        ObteImatge((Persona)p);
                        nCambrers--;
                        gent.AfegeixPersona((Persona)p);
                        p.AllowDrop = false;
                    }
                    else
                    {
                        p = new Posicio();
                        p.Background = Brushes.Beige;
                        p.AllowDrop = true;

                    }
                    p.BorderThickness = new Thickness(2.0);
                    matriu[fila, columna] = p;
                    p.Fila = fila;
                    p.Columna = columna;
                    Grid.SetRow(p, fila);
                    Grid.SetColumn(p, columna);
                    this.Children.Add(p);
                }
            }

            int desordres = (int)Math.Pow((files * columnes), 2);
            for (int i = 0; i < desordres; i++)
            {
                int filaGenerada = r.Next(files);
                int columnaGenerada = r.Next(columnes);

                int filaGenerada2 = r.Next(files);
                int columnaGenerada2 = r.Next(columnes);
                Moure(filaGenerada, columnaGenerada, filaGenerada2, columnaGenerada2);
            }
            this.Drop += new DragEventHandler(Escenari_Drop);
            #region antic
            /*
            this.files = files;
            this.columnes = columnes;
            homes = nHomes;
            dones = nDones;
            cambrers = nCambrers;

            matriu = new Posicio[files,columnes];

            //Creació del tauler
            RowDefinition rd;
            ColumnDefinition cd;
            for(int fila = 0; fila < files; fila++)
            {
                rd = new RowDefinition();
                rd.Height = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star);
                this.RowDefinitions.Add(rd);
            }
            for (int columna = 0; columna < columnes; columna++)
            {
                cd = new ColumnDefinition();
                cd.Width = new GridLength(1, GridUnitType.Star);
                this.ColumnDefinitions.Add(cd);
            }

            //Afegim una posició a cada cassella del tauler
            Posicio p = null;
            Random r = new Random();
            String nom;
            SortedDictionary<String, Int32> simpaties = new SortedDictionary<String, Int32>();
            for (int fila = 0; fila < files; fila++)
            {
                for(int columna = 0; columna < columnes; columna++)
                {
                    if(nHomes > 0)
                    {
                        nom = ObteNom("Noms/NomsNenCurts.txt");
                        while(simpaties.ContainsKey(nom))
                        {
                            nom = ObteNom("Noms/NomsNenCurts.txt");
                        }
                        p = new Home(nom, simpaties, r.Next(3));
                        p.Background = Brushes.Beige;
                        ObteImatge((Persona)p);
                        nHomes--;
                        simpaties.Add(nom, 0);
                        gent.AfegeixPersona((Persona) p);
                        p.AllowDrop = false;
                    }
                    else if(nDones > 0)
                    {
                        nom = ObteNom("Noms/NomsNenaCurts.txt");
                        while (simpaties.ContainsKey(nom))
                        {
                            nom = ObteNom("Noms/NomsNenaCurts.txt");
                        }
                        p = new Dona(nom, simpaties, r.Next(3));
                        p.Background = Brushes.Beige;
                        ObteImatge((Persona)p);
                        nDones--;
                        simpaties.Add(nom, 0);
                        gent.AfegeixPersona((Persona) p);
                        p.AllowDrop = false;
                    }
                    else if(nCambrers > 0)
                    {
                        p = new Cambrer();
                        p.Background = Brushes.Beige;
                        ObteImatge((Persona)p);
                        nCambrers--;
                        gent.AfegeixPersona((Persona) p);
                        p.AllowDrop = false;
                    }
                    else
                    {
                        p = new Posicio();
                        p.Background = Brushes.Beige;
                        p.AllowDrop = true;
                    
                    }
                    p.BorderThickness = new Thickness(2.0);
                    matriu[fila,columna] = p;
                    
                    p.Fila = fila;
                    p.Columna = columna;
                    Grid.SetRow(p, fila);
                    Grid.SetColumn(p, columna);
                    this.Children.Add(p);
                }
            }

            //TODO DESORDENAR. 

            //FUNCIONA
            //Moure(0, 0, 2, 3);
            //Moure(0, 1, 2, 2);
            //Moure(2, 2, 1, 3);
            //NO FUNCIONA
            //Moure(0, 0, 2, 1);
            
            int desordres = (int) Math.Pow((files*columnes),2);
            for (int i = 0; i < desordres; i++)
            {
                int filaGenerada = r.Next(files);
                int columnaGenerada = r.Next(columnes);

                int filaGenerada2 = r.Next(files);
                int columnaGenerada2 = r.Next(columnes);
                Moure(filaGenerada, columnaGenerada, filaGenerada2, columnaGenerada2);
            }
            this.Drop += new DragEventHandler(Escenari_Drop);*/
            #endregion
        }



        #endregion
        #region getters i setters
        public int Files
        {
            get { return files; }
            set { files = value; }
        }
        public int Columnes
        {
            get { return columnes; }
            set { columnes = value; }
        }
        public int Homes
        {
            get { return homes; }
            set { homes = value; }
        }
        public int Dones
        {
            get { return dones; }
            set { dones = value; }
        }
        public int Cambrers
        {
            get { return cambrers; }
            set { cambrers = value; }
        }

        public TaulaPersones Gent
        {
            get
            {
                return gent;
            }
        }
        #endregion
        private void Moure(int filaOrigen, int columnaOrigen, int filaDesti, int columnaDesti)
        {
            if (!(filaOrigen == filaDesti && columnaOrigen == columnaDesti))
            {
                Posicio p = this[filaOrigen, columnaOrigen];
                Posicio p2 = this[filaDesti, columnaDesti];

                this[filaOrigen, columnaOrigen] = p2;
                this[filaDesti, columnaDesti] = p;
                Children.Remove(p);
                Children.Remove(p2);
                p.Fila = filaDesti;
                p.Columna = columnaDesti;
                p2.Fila = filaOrigen;
                p2.Columna = columnaOrigen;
                Grid.SetRow(p, filaDesti);
                Grid.SetColumn(p, columnaDesti);
                Grid.SetRow(p2, filaOrigen);
                Grid.SetColumn(p2, columnaOrigen);
                Children.Add(p);
                Children.Add(p2);
            }
        }

        public void Intercambia(Posicio p1, Posicio p2)
        {
            Posicio aux = p1;

            this.Children.Remove(p2);
            this.Children.Remove(p1);

            p1.Fila = p2.Fila;
            p1.Columna = p2.Columna;
            Grid.SetColumn(p1, p2.Columna);
            Grid.SetRow(p1, p2.Fila);
            this.Children.Add(p1);

            p2.Fila = aux.Fila;
            p2.Columna = aux.Columna;
            Grid.SetColumn(p2, aux.Columna);
            Grid.SetRow(p2, aux.Fila);
            this.Children.Add(p2);

        }

        public Posicio this[int fila, int columna]
        {
            get
            {
                return matriu[fila, columna];
            }
            set
            {
                matriu[fila, columna] = value;
            }
        }
        public bool DestiValid(int fil, int col)
        {

            if (fil > Files || col > Columnes) return false; //comprovar més gran
            else if (fil < 0 || col < 0) return false; //comprovar més petit
            else
            {
                Posicio pos = new Posicio(fil, col);
                return pos.EsBuida; //es sobreescriu
            }

        }
        public void Buida(int fil, int col)
        {
            this[fil, col] = new Posicio(fil, col);
        }
        public void Posa(Persona pers)
        {
            Posicio p = this[pers.Fila, pers.Columna];
            this[pers.Fila, pers.Columna] = pers;
            Children.Remove(p);
            Grid.SetRow(pers, p.Fila);
            Grid.SetColumn(pers, p.Columna);
            Children.Add(pers);
        }

        private string ObteNom(string fitxer)
        {
            StreamReader sr = new StreamReader(fitxer, Encoding.Default, true);
            Random r = new Random();
            int nAleatori;

            nAleatori = r.Next(0, File.ReadLines(fitxer).Count());
            for (int i = 0; i < nAleatori; i++)
            {
                sr.ReadLine();
            }
            return sr.ReadLine();
        }
        private void ObteImatge(Persona p)
        {
            int aleatori;
            if (p is Home)
            {
                aleatori = r.Next(9);
                p.ImgIcona = new BitmapImage(new Uri("/Imatges/Home" + aleatori + ".png", UriKind.Relative));
            }
            else if (p is Dona)
            {
                aleatori = r.Next(9);
                p.ImgIcona = new BitmapImage(new Uri("/Imatges/Dona" + aleatori + ".png", UriKind.Relative));
            }
            else
            {
                p.ImgIcona = new BitmapImage(new Uri("/Imatges/Cambrer.png", UriKind.Relative));
            }
        }
        public void Cicle()
        {
            Persona actual;
            Direccio dir;
            Posicio p;
            for (int fila = 0; fila < files; fila++)
            {
                for (int columna = 0; columna < columnes; columna++)
                {
                    p = this[fila, columna];

                    //Per cada posició, si és una persona, cridem la funció onVaig i el movem cap a la direcció que retorni
                    if (!p.EsBuida)
                    {
                        actual = (Persona)p;
                        if (!actual.Tractat)
                        {
                            dir = actual.OnVaig(this);
                            Debug.WriteLine(actual.Nom + " " + actual.Fila + " " + actual.Columna + " " + dir.ToString());
                            switch (dir)
                            {
                                case Direccio.Est:
                                    Moure(p.Fila, p.Columna, p.Fila, p.Columna + 1);
                                    break;
                                case Direccio.Nord:
                                    Moure(p.Fila, p.Columna, p.Fila - 1, p.Columna);
                                    break;
                                case Direccio.Nordest:
                                    Moure(p.Fila, p.Columna, p.Fila - 1, p.Columna + 1);
                                    break;
                                case Direccio.Noroest:
                                    Moure(p.Fila, p.Columna, p.Fila - 1, p.Columna - 1);
                                    break;
                                case Direccio.Oest:
                                    Moure(p.Fila, p.Columna, p.Fila, p.Columna - 1);
                                    break;
                                case Direccio.Sud:
                                    Moure(p.Fila, p.Columna, p.Fila + 1, p.Columna);
                                    break;
                                case Direccio.Sudest:
                                    Moure(p.Fila, p.Columna, p.Fila + 1, p.Columna + 1);
                                    break;
                                case Direccio.Sudoest:
                                    Moure(p.Fila, p.Columna, p.Fila + 1, p.Columna - 1);
                                    break;
                            }

                            actual.Tractat = true;
                        }

                    }
                }
            }

            foreach (Persona per in gent)
            {
                per.Tractat = false;
            }

        }

        private void Escenari_Drop(object sender, DragEventArgs e)
        {
            Point p = e.GetPosition((Grid)sender);
            String tipus = (String)e.Data.GetData("tipus");
            Persona pers = null;
            String nom;

            int fila = CalcularDropY(p.Y);
            int columna = CalcularDropX(p.X);

            switch (tipus)
            {
                case "AFEGEIX_HOME":
                    nom = ObteNom("Noms/NomsNenCurts.txt");
                    while (nomsPersones.Contains(nom))
                    {
                        nom = ObteNom("Noms/NomsNenCurts.txt");
                    }
                    pers = new Home(nom, 1);
                    pers.Fila = fila;
                    pers.Columna = columna;
                    pers.BorderThickness = new Thickness(2.0);
                    ObteImatge((Persona)pers);
                    pers.Background = Brushes.LightBlue;
                    pers.AllowDrop = false;
                    Posa(pers);
                    gent.AfegeixPersona(pers);
                    break;
                case "AFEGEIX_DONA":
                    nom = ObteNom("Noms/NomsNenaCurts.txt");
                    while (nomsPersones.Contains(nom))
                    {
                        nom = ObteNom("Noms/NomsNenaCurts.txt");
                    }
                    pers = new Dona(nom, 0);
                    pers.Fila = fila;
                    pers.Columna = columna;
                    pers.BorderThickness = new Thickness(2.0);
                    ObteImatge((Persona)pers);
                    pers.Background = Brushes.LightCoral;
                    pers.AllowDrop = false;
                    gent.AfegeixPersona(pers);
                    Posa(pers);
                    break;
                case "AFEGEIX_CAMBRER":
                    pers = new Cambrer();
                    pers.Fila = fila;
                    pers.Columna = columna;
                    pers.BorderThickness = new Thickness(2.0);
                    ObteImatge((Persona)pers);
                    pers.Background = Brushes.LightGray;
                    pers.AllowDrop = false;
                    gent.AfegeixPersona(pers);
                    Posa(pers);
                    break;
                default:
                    pers = (Persona)e.Data.GetData("PERSONA");
                    Moure(pers.Fila, pers.Columna, fila, columna);
                    break;
            }
        }

        public void Elimina(Persona p)
        {
            Posicio pos = new Posicio();
            pos.Fila = p.Fila;
            pos.Columna = p.Columna;
            pos.Background = Brushes.Beige;
            pos.AllowDrop = true;
            pos.BorderThickness = new Thickness(2.0);
            Grid.SetRow(pos, p.Fila);
            Grid.SetColumn(pos, p.Columna);
            matriu[p.Fila, p.Columna] = pos;
            this.Children.Remove(p);
            this.Children.Add(pos);
            gent.EliminaPersona(p);

        }

        private int CalcularDropY(double y)
        {
            double alcadaPosicio = this.ActualHeight / files;
            return (int)Math.Truncate(y / alcadaPosicio);
        }

        private int CalcularDropX(double x)
        {
            double ampladaPosicio = this.ActualWidth / columnes;
            return (int)Math.Truncate(x / ampladaPosicio);
        }


    }
}
