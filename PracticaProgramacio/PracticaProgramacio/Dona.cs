﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PracticaProgramacio
{
    public class Dona : Convidat
    {
        public Dona(String nom, SortedDictionary<String, Int32> simpaties, int sexe) : base(nom, sexe, simpaties)
        {
        }
        public Dona(String nom, int sexe) : base(nom, sexe)
        {
        }

        public override bool EsHome => false;

        public override int Interes(Posicio pos)
        {
            if (pos.EsBuida)
                return 0;
            else if (pos.Fila == fila && pos.Columna == columna)
                return 0;
            else
            {
                Persona per = (Persona)pos;
                if (!per.EsConvidat)
                    return 0;
                else
                {
                    Convidat con = (Convidat)per;
                    if (!con.EsHome)
                        return Simpaties[pos.Nom];
                    else
                        return Simpaties[pos.Nom] + PlusSexe;
                }

            }
        }
    }
}
