﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PracticaProgramacio
{
    public abstract class Persona : Posicio
    {
        private Random r = new Random();
        private bool tractat;

        #region Constructors
        public Persona(String nom, int fil, int col) : base(nom, fil, col) {
            this.MouseDown += new MouseButtonEventHandler(OnPersonaMouseDown);
        }

        private void OnPersonaMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var data = new DataObject();
                data.SetData("PERSONA", this);
                DragDrop.DoDragDrop(this, data, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }
        
        public Persona(String nom) : this(nom, 0, 0) {

        }
        public Persona() : this("", 0, 0) {

        }
        #endregion

        #region Propietats
        public bool Tractat
        {
            get
            {
                return tractat;
            }
            set
            {
                tractat = value;
            }
        }
        public override bool EsBuida
        {
            get
            {
                return false;
            }
        }
        public abstract bool EsConvidat
        {
            get;
        }
        #endregion


        private double Atraccio(int fil, int col, Escenari esc)
        {
            double atraccio = 0;
            double distancia;
            for (int fila = 0; fila < esc.Files; fila++)
            {
                for (int columna = 0; columna < esc.Columnes; columna++)
                {
                    if(!esc[fila, columna].EsBuida && !(fila == this.Fila && columna == this.Columna))//(fila != this.Fila && this.Columna != columna))
                    {
                        distancia = Posicio.Distancia(esc[fil, col], esc[fila, columna]);
                        if (distancia != 0)
                            atraccio += Interes(esc[fila, columna]) / distancia;
                    }                    
                }
            }
            return atraccio;
        
        }

        public Direccio OnVaig(Escenari esc)
        {
            int index = 0;
            double atraccioActual;
            double atracMesGran = double.MinValue;
            int indexAtracMesGran = 0; //Posició de la cassella que més atracció generi, s'inicialitza a 4 perquè es l'index que correspon a la seva posició

            List<Int32> repetits = new List<Int32>();

            for (int i = this.fila-1; i <= this.fila+1; i++)
            {
                for (int j = this.columna - 1; j <= this.columna + 1; j++)
                {
                                 
                    //Comprovem que la fila i la columan estiguin dins del tauler i que la nova posició estigui buida
                    if (i >= 0 && i < esc.Files && j >= 0 && j < esc.Columnes && (esc[i, j].EsBuida || (this.Fila == i && this.Columna==j)))
                    {
                        atraccioActual = this.Atraccio(i, j, esc);
                        if (atraccioActual > atracMesGran)//Si l'atracció es més gran que la que tenim guardada l'actualitzem
                        {
                            atracMesGran = atraccioActual;
                            indexAtracMesGran = index;
                            repetits.Clear();
                        }
                        else if (atraccioActual == atracMesGran) //Si són iguals el guardem a un array per despres elegir quin agafem
                        {                           
                            repetits.Add(index);
                            if (!repetits.Contains(indexAtracMesGran))
                                repetits.Add(indexAtracMesGran);
                        }
                    }
                    index++;
                }
            }
            if(repetits.Count > 0) //Si hi ha repetits elegim un
                indexAtracMesGran = repetits[r.Next(repetits.Count)];
            
            Direccio dir;
            switch(indexAtracMesGran)
            {
                case 0:
                    dir = Direccio.Noroest;
                    break;
                case 1:
                    dir = Direccio.Nord;
                    break;
                case 
                    2: dir = Direccio.Nordest;
                    break;
                case 3:
                    dir = Direccio.Oest;
                    break;
                case 5:
                    dir = Direccio.Est;
                    break;
                case 6:
                    dir = Direccio.Sudoest;
                    break;
                case 7:
                    dir = Direccio.Sud;
                    break;
                case 8:
                    dir = Direccio.Sudest;
                    break;
                default:
                    dir = Direccio.Quiet;
                    break;
            }
            return dir;
        }

        public abstract int Interes(Posicio pos);

        





    }
}
