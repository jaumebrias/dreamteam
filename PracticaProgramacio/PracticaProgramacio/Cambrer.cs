﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaProgramacio
{
    public class Cambrer : Persona
    {
        public Cambrer() : base("Cambrer") { }

        public override bool EsConvidat => false;

        public override int Interes(Posicio pos)
        {
            if (pos.EsBuida)
                return 0;
            else
            {
                Persona per = (Persona)pos;

                if (per.EsConvidat)
                    return 1;
                else
                    return -1;
            }
        }
    }
}
